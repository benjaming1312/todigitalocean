var express = require('express');
var router = express.Router();
var Datastore = require('nedb')
  db = new Datastore
  db.users = new Datastore('db/users.db')
  db.users.loadDatabase()

/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log('users router')
  res.send('respond with a resource');
  db.users.find({}, function (err, docs) {
    console.log(docs)
  })
});

router.get('/:id', function(req, res, next) {
  db.users.find({_id: req.params.id}, function (err, docs) {
    res.send(docs)
  })
});
router.post('/:id', function(req, res, next) {
  var user = {
    name: 'new User' + req.params.id
  }
  db.users.insert(user, function (err, docs) {
    res.send(docs)
  })
});

module.exports = router;
